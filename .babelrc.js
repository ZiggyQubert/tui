module.exports = {
  presets: [
    [
      "airbnb",
      {
        targets: {
          node: "current",
        },
      },
    ],
  ],
  plugins: ["@babel/plugin-proposal-class-properties"],
};
