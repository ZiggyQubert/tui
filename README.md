# @ziggyqubert/tui

This is a text ui library that wraps blessed (in spicific the @terminal-junkies/neo-blessed version), it
provides the raw blessed object as the main/default export with updated / wrapped / new components as named
exports / properties, these are provided as classes instead of constructor functions

## Dependencies

Once included this will attempt to force any subsiquent require calls for 'blessed' to pull `post-blessed`
instead

## Components

See the code for details

### Terminal

There is currently an issue with the terminal not exiting when it is destroyed, this effects Video as well.
