# BIN

This contains bin files for use with the package.json `bin` property

should have a first line of
`#!/usr/bin/env node`
to have the script processed by node

## Publishing

this folder will be published and is not processed through babel, and should point to content in dist

## Babel

can use the following to process at runtime

```
require('@babel/register')();
```
