import { Screen, LogConsole, Box } from '../src';

const screen = new Screen({
  smartCSR: true,
  // warnings: true,
  // debug: true,
  // dump: true,
  title: 'Test UI',
});

let content = '';
for (let x = 0; x < 100; x += 1) {
  content = `${content}\nTest Line ${x}`;
}

const testBox = new Box({
  label: ` TEST `,
  top: '0',
  left: '0',
  scrollable: true,
  autoScroll: true,
  height: '80%',
  width: '100%',
  border: {
    type: 'line',
  },
  help: 'this is help',
  content,
});
screen.append(testBox);

const logConsole = new LogConsole({
  label: ` LOG `,
  top: '80%',
  left: '0',
  height: '20%',
  width: '100%',
  border: {
    type: 'line',
  },
});
screen.append(logConsole);

screen.on('destroy', () => {});

testBox.focus();
screen.render();

setTimeout(() => {
  // eslint-disable-next-line no-console
  console.log('Test Log');
}, 0);
