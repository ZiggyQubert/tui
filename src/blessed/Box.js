import blessed from './index';
import BoxLike from './mixin/BoxLike';
import AutoScrollable from './mixin/AutoScrollable';

export default class extends AutoScrollable(BoxLike(blessed.box)) {}
