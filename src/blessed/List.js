import blessed from './index';
import BoxLike from './mixin/BoxLike';
import AutoScrollable from './mixin/AutoScrollable';

export default class extends AutoScrollable(BoxLike(blessed.list)) {
  shouldAutoScroll() {
    return this.selected === this.items.length - 1;
  }

  onScrollKey(keyName, direction, scrollBy) {
    switch (keyName) {
      case 'home':
        this.select(0);
        break;
      case 'end':
        this.select(this.items.length - 1);
        break;
      default:
        this.move(scrollBy * direction);
        break;
    }
  }
}
