import util from 'util';
import Box from './Box';

const levelColors = {
  log: 'white',
  error: 'red',
  warn: 'yellow',
  debug: 'blue',
};

export default class extends Box {
  constructor(options = {}) {
    // eslint-disable-next-line no-param-reassign
    options.label = options.label ?? ' Log Console ';
    // eslint-disable-next-line no-param-reassign
    options.tags = true;
    // eslint-disable-next-line no-param-reassign
    options.scrollable = true;
    // eslint-disable-next-line no-param-reassign
    options.autoScroll = true;
    super(options);

    // attaches to the logging event
    const logHandeler = this.logWithLevel.bind(this);
    this.on('attach', () => {
      this.screen.on('log', logHandeler);
    });
    this.on('detach', () => {
      this.screen.off('log', logHandeler);
    });
  }

  logWithLevel(level, message) {
    this.log(
      `{${levelColors[level]}-fg}${level}: ${message
        .map((i) => {
          switch (typeof i) {
            case 'string':
              return i;
            default:
              return util.inspect(i, {
                colors: true,
                compact: false,
              });
          }
        })
        .join(' ')}{/}`
    );
  }

  log(lines) {
    this.insertBottom(lines);
    this.screen?.render();
  }
}
