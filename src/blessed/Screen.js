/**
 * wraps the screen element to attach closing keys, tab navigation and changes console.* logging to be hidden,
 */
import blessed from './index';

const screenList = [];

const consoleFcns = ['log', 'error', 'warn', 'debug'];
let mappedFcns = null;

// overwrites the console functions to emit a message to screen objects rather than print to stdout
const mapLogFcns = () => {
  mappedFcns = {};
  consoleFcns.forEach((fcnName) => {
    // eslint-disable-next-line no-console
    mappedFcns[fcnName] = console[fcnName];
    // eslint-disable-next-line no-console
    console[fcnName] = (...args) => {
      screenList.forEach((screen) => {
        screen.emit('log', fcnName, args);
      });
    };
  });
};

// unmapps the above functionality
const unMapLogFcns = () => {
  consoleFcns.forEach((fcnName) => {
    // eslint-disable-next-line no-console
    console[fcnName] = mappedFcns[fcnName];
  });
  mappedFcns = null;
};

const attachToConsole = (screen) => {
  screenList.push(screen);
  if (!mappedFcns) {
    mapLogFcns();
  }
  screen.once('destroy', () => {
    screenList.splice(screenList.indexOf(screen), 1);
    if (screenList.length < 1 && mappedFcns) {
      unMapLogFcns();
    }
  });
};

export default class extends blessed.screen {
  /**
   * inherits from screen
   * @param options
   * @param options.screen
   * @param options.parent
   * @param options.children
   * @param options.program
   * @param options.smartCSR {boolean=true} will be automaticly enabled if not explisitly set to false
   * @param options.fastCSR
   * @param options.useBCE
   * @param options.resizeTimeout
   * @param options.autoPadding
   * @param options.cursor
   * @param options.cursor.artificial
   * @param options.cursor.shape
   * @param options.cursor.blink
   * @param options.cursor.color
   * @param options.log
   * @param options.dump
   * @param options.debug
   * @param options.ignoreLocked {[string]} if bindKeys is set they will be automaticlly added to this list
   * @param options.dockBorders
   * @param options.ignoreDockContrast
   * @param options.fullUnicode
   * @param options.sendFocus
   * @param options.warnings
   * @param options.forceUnicode
   * @param options.input
   * @param options.output
   * @param options.terminal
   * @param options.title
   * @param options.bindKeys {boolean=true} if true then the default key bindings will be set
   * @event log thrown when a console.* log statement is sent, the event recieves (level{string}, message{[any]})
   */
  constructor(options = {}) {
    // eslint-disable-next-line no-param-reassign
    options.smartCSR = options.smartCSR ?? true;
    // eslint-disable-next-line no-param-reassign
    options.bindKeys = options.bindKeys ?? true;
    // if binding keys add the auto bound keys to the ignore list
    if (options.bindKeys) {
      // eslint-disable-next-line no-param-reassign
      options.ignoreLocked = options.ignoreLocked || [];
      options.ignoreLocked.splice(0, 0, ['q', 'C-c', 'tab', 'S-tab']);
    }
    super(options);

    // attaches the screen to the console overwrite
    attachToConsole(this);

    // binds the default keys for top level controls
    if (this.options.bindKeys) {
      this.key(['q', 'C-c'], () => {
        this.destroy();
      });

      this.key(['tab'], () => {
        this.focusNext();
        this.render();
      });

      // doesn't work on windows
      this.key('S-tab', () => {
        this.focusPrev();
        this.render();
      });
    }
  }

  /**
   * updates the log function to emit a logging event as well
   * @param args
   * @return {*}
   */
  log(...args) {
    this.emit('log', 'log', args);
    return super.log(...args);
  }

  /**
   * updates the debug function to emit a logging event as well
   * @param args
   * @return {*}
   */
  debug(...args) {
    this.emit('log', 'debug', args);
    return super.debug(...args);
  }
}
