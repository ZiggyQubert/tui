/**
 * note that there are a couple of issues with terminals -
 * - focusability, they dont respond to focusNext etc...
 * - they dont terminate properly when destroying the screen
 */
import isWindows from 'is-windows';
import blessed from './index';
import Focusable from './mixin/Focusable';
import BoxLike from './mixin/BoxLike';

export default class extends Focusable(BoxLike(blessed.terminal)) {
  constructor(options = {}) {
    // eslint-disable-next-line no-param-reassign
    options.shell =
      options.shell || process.env.SHELL || (isWindows ? 'cmd.exe' : 'sh');

    if (!options.parent) {
      throw new Error('Terminal bust have options.parent set');
    }

    super(options);
  }

  kill() {
    try {
      this.pty.kill();
      process.kill(this.pty.pid, 'SIGINT');
      // eslint-disable-next-line no-empty
    } catch (e) {}

    this.term.destroy();
    super.kill();
  }
}
