import path from 'path';
import isWindows from 'is-windows';
import blessed from './index';
import Focusable from './mixin/Focusable';
import BoxLike from './mixin/BoxLike';
import Terminal from './Terminal';

export default class extends Focusable(BoxLike(blessed.box)) {
  /**
   *
   * @param options
   * @param options.file {string} the file to play
   * @param options.speed {number=1} speed to play the file
   */
  constructor(options = {}) {
    // eslint-disable-next-line no-param-reassign
    options.file = options.file || undefined;
    // eslint-disable-next-line no-param-reassign
    options.speed = options.speed ?? 1;

    super(options);

    const shell = path.resolve(
      path.join(
        __dirname,
        '../../',
        `mplayer/${isWindows ? 'Windows64/mplayer.exe' : 'MacOS/mplayer'}`
      )
    );
    const args = [
      '-vo',
      'caca',
      '-quiet',
      '-speed',
      `${this.options.speed}`,
      options.file,
    ];
    const opts = {
      parent: this,
      left: 0,
      top: 0,
      width: this.width - this.iwidth,
      height: this.height - this.iheight,
      shell,
      args: args.slice(),
    };

    this.now = Date.now() / 1000 || 0;
    this.start = opts.start || 0;
    if (this.start) {
      opts.args.unshift('-ss', String(this.start));
    }

    let { DISPLAY } = process.env;
    delete process.env.DISPLAY;
    this.tty = new Terminal(opts);
    process.env.DISPLAY = DISPLAY;

    this.on('click', () => {
      this.tty.pty.write('p');
    });

    // mplayer/mpv cannot resize itself in the terminal, so we have
    // to restart it at the correct start time.
    this.on('resize', () => {
      this.tty.destroy();

      opts.width = this.width - this.iwidth;
      opts.height = this.height - this.iheight;
      opts.args = args.slice();

      const watched = (Date.now() / 1000 || 0) - this.now;
      this.now = Date.now() / 1000 || 0;
      this.start += watched;
      opts.args.unshift('-ss', String(this.start));

      DISPLAY = process.env.DISPLAY;
      delete process.env.DISPLAY;
      this.tty = new Terminal(opts);
      process.env.DISPLAY = DISPLAY;
      this.screen.render();
    });

    this.tty.on('focus', () => {
      this.onFocus();
      this.fakeFocus();
    });

    this.tty.on('blur', () => {
      this.fakeBlur();
      this.onBlur();
    });
  }
}
