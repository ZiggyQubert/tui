export const boxStyles = {
  fg: 'white',
  bg: 'black',
  label: {
    fg: 'blue',
  },
  border: {
    fg: 'blue',
  },
};

export const scrollableStyle = {
  scrollbar: {
    fg: 'black',
    bg: 'white',
  },
};

export const focusableStyle = {
  focus: {
    label: {
      fg: 'white',
    },
    border: {
      fg: 'white',
    },
  },
};

export const hasHelpStyle = {};
