/**
 * gets the current version of blessed and wraps the require call to always return that version
 * https://www.npmjs.com/package/@terminal-junkies/neo-blessed
 * https://www.npmjs.com/package/post-blessed
 */
// import blessed from '@terminal-junkies/neo-blessed';
import blessed from 'post-blessed';

// updates require to return @terminal-junkies/neo-blessed instead of the base blessed module
const origRequire = module.constructor.prototype.require;
// eslint-disable-next-line func-names
module.constructor.prototype.require = function (path) {
  return origRequire.call(this, path.replace(/^blessed/, 'post-blessed'));
};

export default blessed;
