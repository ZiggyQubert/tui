import Scrollable from './Scrollable';

export default (base) =>
  class extends Scrollable(base) {
    constructor(options = {}) {
      if (options.scrollable) {
        // eslint-disable-next-line no-param-reassign
        options.autoScroll = options.autoScroll ?? false;
      }
      super(options);

      if (this.options.autoScroll) {
        // with auto scroll set, scroll to bottom when attached
        this.on('attach', () => {
          setTimeout(() => {
            this.setScrollPerc(100);
          }, 0);
        });
      }
    }

    shouldAutoScroll() {
      return (
        this.options.scrollable &&
        this.options.autoScroll &&
        this.parent &&
        (this.getScrollPerc() >= 100 || this.getScrollHeight() < this.height)
      );
    }

    insert(...args) {
      const shouldScroll = this.shouldAutoScroll();
      super.insert(...args);
      if (shouldScroll) {
        this.setScrollPerc(100);
      }
    }

    insertLine(...args) {
      const shouldScroll = this.shouldAutoScroll();
      super.insertLine(...args);
      if (shouldScroll) {
        this.setScrollPerc(100);
      }
    }
  };
