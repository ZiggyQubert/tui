/* eslint-disable no-underscore-dangle */
import deepmerge from 'deepmerge';
import { boxStyles } from '../defaultStyles';

export default (base) =>
  class extends base {
    constructor(options = {}) {
      // eslint-disable-next-line no-param-reassign
      options.style = deepmerge(boxStyles, options.style || {});

      // convience to allow for border to be set to true
      if (options.border === true) {
        // eslint-disable-next-line no-param-reassign
        options.border = {
          type: 'line',
        };
      }
      super(options);
    }
  };
