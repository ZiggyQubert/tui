/* eslint-disable no-underscore-dangle */
import deepmerge from 'deepmerge';
import { focusableStyle } from '../defaultStyles';

export default (base) =>
  class extends base {
    constructor(options = {}) {
      // eslint-disable-next-line no-param-reassign
      options.style = deepmerge(focusableStyle, options.style || {});

      super(options);

      this.on('focus', this.onFocus);
      this.on('blur', this.onBlur);
    }

    onFocus() {
      this.super?.onFocus();
      const labelStyle = this.options.style?.focus?.label;
      if (labelStyle && this._label) {
        this._label.style = labelStyle;
      }
    }

    onBlur() {
      this.super?.onBlur();
      const labelStyle = this.options.style?.label;
      if (labelStyle && this._label) {
        this._label.style = labelStyle;
      }
    }

    fakeFocus() {
      this.style = { ...this.options.style, ...this.options.style.focus };
    }

    fakeBlur() {
      this.style = { ...this.options.style };
    }
  };
