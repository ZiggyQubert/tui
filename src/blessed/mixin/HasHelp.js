/* eslint-disable no-underscore-dangle */
import deepmerge from 'deepmerge';
import { hasHelpStyle } from '../defaultStyles';
import blessed from '../index';
// import Box from '../Box';

const nextTick = global.setImmediate || process.nextTick.bind(process);

export default (base) =>
  class extends base {
    constructor(options = {}) {
      // eslint-disable-next-line no-param-reassign
      options.style = deepmerge(hasHelpStyle, options.style || {});
      // eslint-disable-next-line no-param-reassign
      options.help = options.help || undefined;

      super(options);

      if (this.options.help) {
        this.setHelp(` ${this.options.help} `);
      }
    }

    setHelp(options) {
      const Box = blessed.box;

      if (typeof options === 'string') {
        // eslint-disable-next-line no-param-reassign
        options = { text: options };
      }

      if (this._help) {
        this._help.setContent(options.text);
        if (options.side !== 'right') {
          this._help.rleft = 2 + (this.border ? -1 : 0);
          this._help.position.right = undefined;
          if (!this.screen.autoPadding) {
            this._help.rleft = 2;
          }
        } else {
          this._help.rright = 2 + (this.border ? -1 : 0);
          this._help.position.left = undefined;
          if (!this.screen.autoPadding) {
            this._help.rright = 2;
          }
        }
        return;
      }

      this._help = new Box({
        screen: this.screen,
        parent: this,
        content: options.text,
        top: '100%-1',
        tags: this.parseTags,
        shrink: true,
        style: this.style.label,
      });

      if (options.side !== 'right') {
        this._help.rleft = 2 - this.ileft;
      } else {
        this._help.rright = 2 - this.iright;
      }

      this._help._isLabel = true;

      if (!this.screen.autoPadding) {
        if (options.side !== 'right') {
          this._help.rleft = 2;
        } else {
          this._help.rright = 2;
        }
        this._help.rtop = 0;
      }

      const reposition = () => {
        this._help.rtop = (this.childBase || 0) - this.itop + this.height - 1;
        if (!this.screen.autoPadding) {
          this._help.rtop = this.childBase || 0;
        }
        this.screen.render();
      };

      this.on(
        'scroll',
        (this._helpScroll = () => {
          reposition();
        })
      );

      this.on(
        'resize',
        (this._helpResize = () => {
          nextTick(() => {
            reposition();
          });
        })
      );
    }

    removeHelp() {
      if (!this._help) return;
      this.removeListener('scroll', this._helpScroll);
      this.removeListener('resize', this._helpResize);
      this._help.detach();
      delete this._helpScroll;
      delete this._helpResize;
      delete this._help;
    }

    // onFocus() {
    //   this.super?.onFocus();
    //   const labelStyle = this.options.style?.focus?.label;
    // }
    //
    // onBlur() {
    //   this.super?.onBlur();
    //   const labelStyle = this.options.style?.focus?.label;
    // }
  };
