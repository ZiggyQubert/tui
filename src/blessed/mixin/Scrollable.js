import deepmerge from 'deepmerge';
import { scrollableStyle } from '../defaultStyles';
import Focusable from './Focusable';

export default (base) =>
  class extends Focusable(base) {
    constructor(options = {}) {
      if (options.scrollable) {
        // eslint-disable-next-line no-param-reassign
        options.alwaysScroll = options.alwaysScroll ?? true;
        // eslint-disable-next-line no-param-reassign
        options.scrollbar = options.scrollbar ?? true;
        // eslint-disable-next-line no-param-reassign
        options.keys = options.keys ?? true;
        // eslint-disable-next-line no-param-reassign
        options.style = deepmerge(scrollableStyle, options.style || {});
        // eslint-disable-next-line no-param-reassign
        options.style.scrollbar.fg =
          options.style.bg || options.style.scrollbar.fg;
        // eslint-disable-next-line no-param-reassign
        options.style.scrollbar.bg =
          options.style.fg || options.style.scrollbar.bg;
      }
      super(options);

      this.bindScrollKeys();
    }

    bindScrollKeys() {
      this.key(['pageup', 'pagedown', 'home', 'end'], (e, key) => {
        const direction = ['pageup', 'home'].indexOf(key.full) > -1 ? -1 : 1;
        const scrollBy = this.height - this.iheight;
        this.onScrollKey(key.full, direction, scrollBy);
        this.screen.render();
      });
    }

    onScrollKey(keyName, direction, scrollBy) {
      switch (keyName) {
        case 'home':
          this.setScrollPerc(0);
          break;
        case 'end':
          this.setScrollPerc(100);
          break;
        default:
          this.scroll(scrollBy * direction);
          break;
      }
    }
  };
