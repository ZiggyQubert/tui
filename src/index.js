/* eslint-disable no-underscore-dangle */
import blessed from './blessed';
import Screen from './blessed/Screen';
import LogConsole from './blessed/LogConsole';
import Box from './blessed/Box';
import Terminal from './blessed/Terminal';
import Video from './blessed/Video';
import List from './blessed/List';
import Text from './blessed/Text';

// fixes an issue where getting the width / height throws an error if not attached to anything
const origGetHeight = blessed.element.prototype._getHeight;
// eslint-disable-next-line func-names
blessed.element.prototype._getHeight = function (...args) {
  if (
    !this.parent &&
    (typeof this.position.height === 'string' || this.position.height === null)
  ) {
    if (!this._resizeAttached) {
      this._resizeAttached = this.once('attach', () => {
        // this.emit('resize');
        delete this._resizeAttached;
      });
    }
    return this.iheight;
  }
  return origGetHeight.apply(this, args);
};
const origGetWidth = blessed.element.prototype._getWidth;
// eslint-disable-next-line func-names
blessed.element.prototype._getWidth = function (...args) {
  if (
    !this.parent &&
    (typeof this.position.width === 'string' || this.position.width === null)
  ) {
    if (!this._resizeAttached) {
      this._resizeAttached = this.once('attach', () => {
        // this.emit('resize');
        delete this._resizeAttached;
      });
    }
    return this.iwidth;
  }
  return origGetWidth.apply(this, args);
};

// debounces the keypress event to avoid duplicates
let prevKeypress = null;
const origEmit = blessed.program.prototype.emit;
// eslint-disable-next-line func-names
blessed.program.prototype.emit = function (...args) {
  // look at only the keypress events
  if (args[0] === 'key' || args[0] === 'keypress') {
    const currentKeypress = JSON.stringify(args[2]);
    if (prevKeypress === currentKeypress) {
      return;
    }
    prevKeypress = currentKeypress;
    setTimeout(() => {
      if (prevKeypress === currentKeypress) {
        prevKeypress = null;
      }
    }, 0);
  }
  origEmit.apply(this, args);
};

export { Screen, LogConsole, Box, Terminal, Video, List, Text };

export default blessed;
