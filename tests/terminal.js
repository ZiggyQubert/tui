import { Screen, LogConsole, Terminal } from '../src';

const screen = new Screen({
  smartCSR: true,
  // warnings: true,
  // debug: true,
  // dump: true,
  title: 'Test UI',
});

const testBox = new Terminal({
  parent: screen,
  label: ` TERMINAL `,
  top: '0',
  left: '0',
  scrollable: true,
  autoScroll: true,
  height: '80%',
  width: '100%',
  border: {
    type: 'line',
  },
});
screen.append(testBox);

const logConsole = new LogConsole({
  label: ` LOG `,
  top: '80%',
  left: '0',
  height: '20%',
  width: '100%',
  border: {
    type: 'line',
  },
});
screen.append(logConsole);

logConsole.focus();
screen.render();
