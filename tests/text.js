import { Screen, LogConsole, Text } from '../src';

const screen = new Screen({
  smartCSR: true,
  // warnings: true,
  // debug: true,
  // dump: true,
  title: 'Test UI',
});

const testBox = new Text({
  parent: screen,
  top: '0',
  left: '0',
  scrollable: true,
  autoScroll: true,
  content: 'This is some text content',
  height: '80%',
  width: '100%',
});
screen.append(testBox);

const logConsole = new LogConsole({
  label: ` LOG `,
  top: '80%',
  left: '0',
  height: '20%',
  width: '100%',
  border: {
    type: 'line',
  },
});
screen.append(logConsole);

logConsole.focus();
screen.render();
